# Configuración emacs

Proyecto con configuración para emacs.

# Instalación.

1. Poner el siguiente código en el archivo `.emacs`

		;; 1. Package manager
		;; ===================
		(require 'package)
		(add-to-list 'package-archives '("elpa"      . "http://tromey.com/elpa/"))
		(add-to-list 'package-archives '("gnu"       . "http://elpa.gnu.org/packages/"))
		(add-to-list 'package-archives '("melpa"     . "http://melpa.org/packages/"))
		(add-to-list 'package-archives '("marmalade" . "https://marmelade-repo.org/packages"))
		(package-initialize)


2. Instalar los paquetes

	* seti-theme
	* web-mode
	* markdown-mode


3. Añadir el siguiente código en `.emacs`

		(load XXX)

donde XXX es la ruta en la que está el archivo `dpa-init.el` de este repositorio
