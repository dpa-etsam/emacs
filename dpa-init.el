;; Archivo de configuración de emacs
;; Índice
;; 1. Package manager
;; 2. Modes
;;    2.1 Web mode
;; =====================================================

;; Codificación en UTF-8
(prefer-coding-system 'utf-8)

;; Permite seleccionar varios cursores
(require 'multiple-cursors)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)

;; Permite transponer líneas con M-up y M-down
(require 'move-text)
(move-text-default-bindings)

;; Al escribir sobre un texto seleccionado, se borra
(delete-selection-mode 1)

;; Activa el undo-tree. Un modo muy genial.
(require 'undo-tree)
(undo-tree-mode 1)

;; Muestra el número de columna además de la fila
(column-number-mode 1)

;; 2. Modes
;; =========

;; Delete Selection Mode
;; Replace the active region typing text.
(setq css-indent-offset 2)


;; Web mode and php mode
;; ----------------------
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))


(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-enable-current-element-highlight t)
  (setq web-mode-enable-current-column-highlight t)
  (setq indent-tabs-mode t)
  (setq-default tab-width 2)
)
(add-hook 'web-mode-hook  'my-web-mode-hook)

(add-to-list 'web-mode-indentation-params '("lineup-args" . nil))
(add-to-list 'web-mode-indentation-params '("lineup-calls" . nil))
(add-to-list 'web-mode-indentation-params '("lineup-concats" . nil))
(add-to-list 'web-mode-indentation-params '("lineup-ternary" . nil))

(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)



;; 2.2 Markdown mode
;; ------------------
(require 'markdown-mode)
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
