#+TITLE:  REGISTRO DE INCIDENCIAS DE INFORM�TICA DPA
#+AUTHOR: Carlos Saito
#+DATE:   29/10/2015

* TODO [#A] M�ster Habilitante.
  P�gina de la Ficha PFC-TFM del M�ster Habilitante.
  
** DONE Crear informes imprimibles
   Informes para que se puedan imprimir uno a uno

** DONE Crear el formulario de preinscripci�n
   Para que los alumnos que deseen cursar el m�ster habilitante puedan preinscribirse



* TODO (17/11/2015) Aula Aranguren B http://dpa-etsam.aq.upm.es/blogs/aulaaranguren-pfctfm
  Tareas relacionadas con el blog del Aula PFC-TFM Aranguren 

** TODO [#C] Quitar el bot�n de b�squeda en el men�
   Modificar CSS para quitar el bot�n de b�squeda en el men�




* TODO [#B] (17/11/2015) Criticall.
  Tareas relacionadas con la web de Critic|all

** TODO [#C] Tipograf�a Impact
   Subirla a la web para que se visualice en todos los dispositivos

** DONE GIF
   Actualizado para que no aparezca la marca de agua

** TODO [#C] Compatibilizar CSS del efecto plata para que sea compatible con prefijos
   Modificar archivo .js del tema para que el CSS incluya prefijos de navegador


